import { Injectable } from '@nestjs/common';
import { Item } from './interfaces/item.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ItemModel } from './models';
import { ItemSchemaName } from './schemas/item.schema';
import { CreateItemDto } from './dto/create-item.dto';
import { UpdateItemDto } from './dto/update-item.dto';

@Injectable()
export class ItemsService {

    constructor(@InjectModel(ItemSchemaName) private readonly itemModel: Model<ItemModel>) { }

    async findAll(): Promise<Item[]> {
        return this.itemModel.find();
    }

    async findOne(id: string): Promise<ItemModel> {
        return this.itemModel.findOne({ _id: id })
    }

    async create(dto: CreateItemDto, userId: string): Promise<ItemModel> {
        return this.itemModel.create({ ...dto, user: userId })
    }

    async update(id: string, dto: UpdateItemDto): Promise<ItemModel> {
        return this.itemModel.findByIdAndUpdate(id, dto, { new: true });
    }

    async delete(id: string): Promise<ItemModel> {
        return this.itemModel.findByIdAndRemove(id);
    }


}
