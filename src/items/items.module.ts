import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ItemsController } from '../items/items.controller';
import { ItemsService } from '../items/items.service';
import { ItemSchema, ItemSchemaName } from '../items/schemas/item.schema';
import { UsersModule } from 'src/users/users.module';
@Module({
  imports: [MongooseModule.forFeature([{ name: ItemSchemaName, schema: ItemSchema }]), UsersModule],
  controllers: [ItemsController],
  providers: [ItemsService],
})
export class ItemsModule { }
