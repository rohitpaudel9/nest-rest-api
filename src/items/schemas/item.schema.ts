import * as mongoose from 'mongoose';
import { UserSchemaName } from 'src/users/schemas/user.schema';
export const ItemSchemaName: string = 'item';
export const ItemSchema = new mongoose.Schema({
    name: String,
    quantity: Number,
    description: String,
    user: { type: String, required: true, ref: UserSchemaName }
});     