import { Controller, Get, Post, Put, Delete, Body, Param, HttpException, HttpStatus } from '@nestjs/common';
import { CreateItemDto } from './dto/create-item.dto';
import { UpdateItemDto } from './dto/update-item.dto';
import { ItemsService } from './items.service';
import { Item } from './interfaces/item.interface';
import { UsersService } from 'src/users/users.service';

@Controller('items')
export class ItemsController {
    constructor(private readonly itemsService: ItemsService, private readonly usersService: UsersService) { }

    @Get()
    async findAll(): Promise<Item[]> {
        return this.itemsService.findAll();
    }

    @Get(':id')
    async findOne(@Param('id') id): Promise<Item> {
        return this.itemsService.findOne(id);
    }

    @Post(':userId')
    async create(@Body() dto: CreateItemDto, @Param('userId') userId: string): Promise<Item> {
        if (await this.usersService.checkId(userId)) {
            return this.itemsService.create(dto, userId);
        }
    }

    @Put(':id')
    async update(@Body() dto: UpdateItemDto, @Param('id') id): Promise<Item> {
        return this.itemsService.update(id, dto)
    }

    @Delete(':id')
    async delete(@Param('id') id): Promise<Item> {
        return this.itemsService.delete(id);
    }
}
