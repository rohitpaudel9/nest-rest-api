import { Document } from "mongoose";

export class UpdateItemDto extends Document {
    readonly name: string;
    readonly description: string;
    readonly quantity: number;
}