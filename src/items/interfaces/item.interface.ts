
export interface Item {
    name: string,
    description?: string,
    quantity: number,
    user: string,
}