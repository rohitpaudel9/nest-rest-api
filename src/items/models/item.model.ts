import { Item } from "../interfaces/item.interface";
import { Document } from "mongoose";

export interface ItemModel extends Item, Document { }