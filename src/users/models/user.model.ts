import { User } from "../interfaces/user.interface";
import { Document } from "mongoose";

export interface UserModel extends User, Document { }