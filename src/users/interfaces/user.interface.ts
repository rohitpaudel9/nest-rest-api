export interface User {
    firstname: string,
    lastname: string,
    email: string,
    dob: Date,
}