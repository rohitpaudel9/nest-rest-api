import { Document } from "mongoose";

export class UpdateUserDto extends Document {
    readonly firstname: string;
    readonly lastname: string;
    readonly email: string;
    readonly dob: Date;
}