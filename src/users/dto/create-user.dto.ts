import { Document } from 'mongoose';
import { IsEmail, IsString, IsNotEmpty, IsOptional, IsDate } from 'class-validator';

export class CreateUserDto extends Document {
    @IsString()
    @IsNotEmpty()
    readonly firstname: string;

    @IsString()
    @IsNotEmpty()
    readonly lastname: string;

    @IsEmail()
    @IsNotEmpty()
    readonly email: string;

    @IsOptional()
    @IsDate()
    readonly dob: Date;

}