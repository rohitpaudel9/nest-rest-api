import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UserSchemaName } from './schemas/user.schema';
import { Model } from 'mongoose';
import { User } from './interfaces/user.interface';
import { UserModel } from './models/user.model';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {

    constructor(@InjectModel(UserSchemaName) private readonly userModel: Model<UserModel>) { }

    async findAll(): Promise<User[]> {
        return this.userModel.find();
    }

    async findOne(id: string): Promise<UserModel> {
        return this.userModel.findOne({ _id: id });
    }

    async create(dto: CreateUserDto): Promise<UserModel> {
        return this.userModel.create({ ...dto })
    }

    async update(id: string, dto: UpdateUserDto): Promise<UserModel> {
        return this.userModel.findByIdAndUpdate(id, dto, { new: true });
    }

    async delete(id: string): Promise<UserModel> {
        return this.userModel.findByIdAndRemove(id);
    }

    checkId(userId: string): Promise<Boolean> {
        return this.userModel.exists({ _id: userId })
    }
}
