import * as mongoose from 'mongoose';
export const UserSchemaName: string = 'user';
export const UserSchema = new mongoose.Schema({
    firstname: String,
    lastname: String,
    email: String,
    dob: Date,
})