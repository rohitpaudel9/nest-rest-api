import * as dotenv from 'dotenv';

dotenv.config();
const env: any = process.env;

export const environment: any = {
    applicationName: env.APP_NAME,
    mongodb: env.MONGODB_URL,
    port: env.PORT,
};